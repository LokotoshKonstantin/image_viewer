import sys
import os
import cv2 as cv

from ImageViewer.image_viewer import list_images, make_histogram_image

max_index_of_images = 1
list_of_image_name = []


def on_trackbar(val):
    try:
        current_image = cv.imread(list_of_image_name[val])
        cv.imshow("Image Viewer", current_image)
        cv.imshow("Histogram", make_histogram_image(current_image))
    except:
        print("Error opening file:" + list_of_image_name[val])


def main():
    if len(sys.argv) != 2:
        sys.exit("Invalid number of arguments. Use:imageviewer <path>")
    if not os.path.exists(sys.argv[1]):
        sys.exit("Invalid argument <path>. Example:imageviewer /home/.../Pictures")
    global list_of_image_name
    global max_index_of_images
    list_of_image_name = list_images(sys.argv[1])
    count_images_in_dir = len(list_of_image_name)
    max_index_of_images = count_images_in_dir - 1

    if count_images_in_dir > 0:
        cv.namedWindow("Histogram", cv.WINDOW_KEEPRATIO)
        cv.resizeWindow("Histogram", 256, 256)

        cv.namedWindow("Image Viewer", cv.WINDOW_KEEPRATIO)
        cv.resizeWindow("Image Viewer", 256, 256)

        if count_images_in_dir > 1:
            trackbar_name = 'Current image index'
            cv.createTrackbar(trackbar_name, "Image Viewer", 0, max_index_of_images, on_trackbar)
        on_trackbar(0)
    else:
        sys.exit("Not found images in this directory.")

    cv.waitKey()

# For use program write in terminal:imageviewer <path>
if __name__ == "__main__":
    main()
