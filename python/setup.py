from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='image_viewer',
    version='1.0',
    description='Python program using the OpenCV library to view images in a given directory',
    author="Lokotosh Konstantin",
    author_email="lokotoch777@mail.ru",
    long_description=open(join(dirname(__file__), 'README.txt')).read(),
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "opencv-python", "numpy"
    ],
    entry_points={
    	"console_scripts": [
    		"imageviewer = ImageViewer.main:main"]
    },
)
