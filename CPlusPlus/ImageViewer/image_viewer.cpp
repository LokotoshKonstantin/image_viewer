#include "image_viewer.h"

namespace fs = std::experimental::filesystem;
using namespace std;
using namespace cv;

//Returns the vector of image file paths in the specified directory(src_path).
vector<string> ImageViewer::list_images(const string& src_path){
  fs::directory_iterator begin(src_path);
  fs::directory_iterator end;
  vector<string> image_files;

  string ext;
  fs::path current_path;
  for (auto it = begin; it != end; it++) {
    current_path = it->path();
    ext = current_path.extension();

    if (fs::is_regular_file(current_path) && ((ext == ".jpg") || (ext == ".jpeg") || (ext == ".png"))){
      image_files.push_back(current_path.u8string());
    }
  }

  return image_files;
}

//Returns cv:Mat of image histogram
Mat ImageViewer::make_histogram_image(const Mat& src_image){
  vector<Mat> bgr_planes;
  split( src_image, bgr_planes );
  int histSize = 256;
  float range[] = { 0, 256 };
  const float* histRange = { range };
  bool uniform = true, accumulate = false;
  Mat b_hist, g_hist, r_hist;
  calcHist( &bgr_planes[0], 1, 0, Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate );
  calcHist( &bgr_planes[1], 1, 0, Mat(), g_hist, 1, &histSize, &histRange, uniform, accumulate );
  calcHist( &bgr_planes[2], 1, 0, Mat(), r_hist, 1, &histSize, &histRange, uniform, accumulate );
  int hist_w = 256, hist_h = 256;
  int bin_w = cvRound( (double) hist_w/histSize );
  Mat hist_image( hist_h, hist_w, CV_8UC3, cv::Scalar( 0,0,0) );
  normalize(b_hist, b_hist, 0, hist_image.rows, NORM_MINMAX, -1, Mat() );
  normalize(g_hist, g_hist, 0, hist_image.rows, NORM_MINMAX, -1, Mat() );
  normalize(r_hist, r_hist, 0, hist_image.rows, NORM_MINMAX, -1, Mat() );
  for( int i = 1; i < histSize; i++ )
  {
    line( hist_image, Point( bin_w*(i-1), hist_h - cvRound(b_hist.at<float>(i-1)) ),
          Point( bin_w*(i), hist_h - cvRound(b_hist.at<float>(i)) ),
          Scalar( 255, 0, 0), 2, 8, 0  );
    line( hist_image, Point( bin_w*(i-1), hist_h - cvRound(g_hist.at<float>(i-1)) ),
          Point( bin_w*(i), hist_h - cvRound(g_hist.at<float>(i)) ),
          Scalar( 0, 255, 0), 2, 8, 0  );
    line( hist_image, Point( bin_w*(i-1), hist_h - cvRound(r_hist.at<float>(i-1)) ),
          Point( bin_w*(i), hist_h - cvRound(r_hist.at<float>(i)) ),
          Scalar( 0, 0, 255), 2, 8, 0  );
  }
  return hist_image;
}