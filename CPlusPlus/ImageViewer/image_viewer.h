#ifndef IMAGEVIEWER__IMAGE_VIEWER_H_
#define IMAGEVIEWER__IMAGE_VIEWER_H_

#include <iostream>
#include <vector>
#include <experimental/filesystem>
#include <iterator>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/imgproc.hpp>

class ImageViewer {
 public:
  static std::vector<std::string> list_images(const std::string& path);
  static cv::Mat make_histogram_image(const cv::Mat& src_image);
};

#endif //IMAGEVIEWER__IMAGE_VIEWER_H_
