#include <iostream>
#include "image_viewer.h"

namespace fs = std::experimental::filesystem;
using namespace std;
using namespace cv;

int max_index_of_images = 10;
int index_of_image;
vector<string> list_of_image_name;

static void on_trackbar( int, void* )
{
  try {
    Mat current_image = imread(list_of_image_name[index_of_image]);
    imshow( "Histogram", ImageViewer::make_histogram_image(current_image));
    imshow( "Image Viewer", current_image);
  }
  catch (const Exception& e){
    cout << "Error opening file:" + list_of_image_name[index_of_image] << endl;
  }
}

int main(int argc, char** argv) {
  if ( argc != 2 )
  {
    cout << "Invalid parameters. Use:./ImageViewer <path>" << endl;
    return -1;
  }

  if (!fs::exists(argv[1])){
    cout << "Invalid parameter <path>. Example: ./ImageViewer /home/.../Pictures" << endl;
    return -1;
  }

  list_of_image_name = ImageViewer::list_images(argv[1]);
  int count_images_in_dir = list_of_image_name.size();
  max_index_of_images = count_images_in_dir - 1;
  index_of_image = 0;


  if (count_images_in_dir > 0){
    namedWindow("Histogram", cv::WINDOW_NORMAL);
    resizeWindow("Histogram", 256, 256);

    namedWindow("Image Viewer", cv::WINDOW_NORMAL);
    resizeWindow("Image Viewer", 256, 256);

    if (count_images_in_dir > 1){
      char TrackbarName[50];
      sprintf( TrackbarName, "Current image index %d", max_index_of_images );
      createTrackbar( TrackbarName, "Image Viewer", &index_of_image, max_index_of_images, on_trackbar);
    }
    on_trackbar( index_of_image, nullptr );
  }
  else{
    cout << "Not found images in this directory." << endl;
    return -1;
  }

  waitKey(0);
  return 0;
}
